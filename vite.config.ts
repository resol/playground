import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { VitePWA } from 'vite-plugin-pwa'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    VitePWA({
      registerType: 'autoUpdate',
      injectRegister: 'auto',
      devOptions: {
        enabled: true
      },
      workbox: {
        globPatterns: ['**/*.{js,css,html,ico,png,svg}']
      },
      includeAssets: ["favicon.png"],
      manifest: {
        name: "reŠOL playground",
        short_name: "reŠOL playground",
        description: "Playground pro testování reŠOL API",
        theme_color: "#313244",
        icons: [
          {
            src: "favicon.png",
            sizes: "500x500",
            type: "image/png",
          },
        ],
      }
    })
  ],
})
