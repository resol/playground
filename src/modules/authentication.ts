export function isAuthenticated() {
  const authHeader = localStorage.getItem("authHeader");
  const solUrl = localStorage.getItem("solUrl");
  
  return authHeader != null && solUrl != null;
}

