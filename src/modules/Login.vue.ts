import { store, loginData, userData } from "../store.ts"

export function logout() {
  localStorage.clear()
  loginData.authenticated = false;
  
  window.location.hash = "#"
}

export async function fetchData() {
  if (store.lock) return 

  store.status = "";
  loginData.loginDisabled = true;

  store.lock = true;
  let headers = new Headers({
    "sol-url": store.solUrl,
    "Authorization": `Basic ${btoa(`${store.username}:${store.password}`)}`
  })

  const auth = {
    headers: headers
  }

  store.status = "Logging in.. (creating request at https://re-sol.tech/api/v1/verify)";
  const response = await fetch("https://re-sol.tech/api/v1/verify", auth)
  const json = await response.json();

  if (!json["error"]) {
    localStorage.setItem("authHeader", btoa(`${store.username}:${store.password}`));
    localStorage.setItem("solUrl", store.solUrl);

    store.status = "Logged in! Getting some user data.. (creating request at https://re-sol.tech/api/v1/user)";

    const userDataResponse = await fetch("https://re-sol.tech/api/v1/user", auth)
    const userDataJson = await userDataResponse.json();
    localStorage.setItem("studentId", userDataJson["personID"])

    console.log(userDataJson)
    userData.user = userDataJson["fullName"]
    userData.studyYear = userDataJson["studyYear"]

    store.status = "Logging in..";
    store.userDataAvailable = true;

    loginData.authenticated = true;
    window.location.hash = "#/dashboard"

    store.status = "You're not logged in."
  } else {
    store.status = json["message"];
  }

  store.lock = false;
  loginData.loginDisabled = false;
}

export function fillData() {
  const username = localStorage.getItem("username");
  const password = localStorage.getItem("password");
  const solUrl = localStorage.getItem("solUrl");
  
  username ? store.username = username : {};
  password ? store.password = password : {};
  solUrl ? store.solUrl = solUrl : {};
}
