import { reactive } from 'vue'

export const store = reactive({
  username: "", 
  password: "", 
  solUrl: "", 
  status: "You're not logged in.", 
  lock: false,
  userDataAvailable: false,
})

export const loginData = reactive({
  authenticated: false,
  loginDisabled: false
})

export const userData = reactive({
  user: "",
  studyYear: 0
})
